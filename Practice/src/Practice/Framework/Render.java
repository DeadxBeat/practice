package Practice.Framework;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public abstract class Render {
	
	private static UnicodeFont font;
	
	public static int shaderProgram;
	
	private static Triangle triangle = new Triangle();
	
	@SuppressWarnings("unchecked")
	private static void setupFont() {
		java.awt.Font awtFont = new java.awt.Font("Arial", java.awt.Font.PLAIN, 22);
		font = new UnicodeFont(awtFont);
		font.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		font.addAsciiGlyphs();
		try {
			font.loadGlyphs();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static void initGL() {
		System.out.println("Starting using OpenGL Version: " + glGetString(GL_VERSION));
		setupFont();
		Shaders.loadShaders();
		Camera.setPerspective();
		glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    	glShadeModel(GL_SMOOTH);
        glClearDepth(1.0);
        glClearColor(0.3f,0.3f,0.3f, 1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	}
	
	public static void render() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		Camera.lookAt(Mouse.getDX(), Mouse.getDY());
		Camera.update();

		// Draw stuff here
		triangle.draw();
		// Stop Drawing
		
		Frustum.getFrustum();
		displayHUD();
	}
	
	private static void displayHUD() {
		Camera.setOrtho();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glPushMatrix();
		  glLoadIdentity();
		  //glTranslatef(Mouse.getX(), Display.getHeight() - Mouse.getY(), 0);
		  glBegin(GL_LINES);
		  	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		    glVertex2f(Display.getWidth()/2, Display.getHeight()/2-10);
		    glVertex2f(Display.getWidth()/2, Display.getHeight()/2+10);
		    glVertex2f(Display.getWidth()/2-10, Display.getHeight()/2);
		    glVertex2f(Display.getWidth()/2+10, Display.getHeight()/2);
		  glEnd();
		  
		  glPushMatrix();
		    glLoadIdentity();
		    font.drawString(10, 10, "Camera Position    X:" + (int)Camera.camPos.x + "    Y:" + (int)Camera.camPos.y + "    Z:" + (int)Camera.camPos.z);
		    font.drawString(10, 30, "Camera Rotation    Pitch:" + (int)Camera.camRot.x + "    Yaw:" + (int)Camera.camRot.y + "    Roll:" + (int)Camera.camRot.z);
		    //font.drawString(10, 50, "Subdivisions: " + p.level);
		    glDisable(GL_TEXTURE_2D);
		  glPopMatrix();
		glPopMatrix();
		Camera.setPerspective();
		glMatrixMode(GL_MODELVIEW);
	}

}
