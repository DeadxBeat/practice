package Practice.Framework;

import org.lwjgl.*;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.*;

import Practice.Framework.Enums.Direction;

public class Main {

	private static boolean closeRequested = false;
	private static long lastFrame;
	private static int fps;
	private static long lastFPS;
	
	private static void initDisplay() {
		try {
			Display.setDisplayMode(new DisplayMode(1280, 800));
			Display.setTitle("Practice");
			Display.setResizable(false);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	private static void mainLoop() {
		initDisplay();
		Render.initGL();
		lastFPS = getTime();
		while(!Display.isCloseRequested() && !closeRequested) {
			updateFPS();
			checkInput();
			Render.render();
			
			Display.sync(60);
			Display.update();
		}
		
		Display.destroy();
		System.exit(0);
		
	}
	
	private static void checkInput() {
		if(Mouse.isButtonDown(0)) {
			Mouse.setGrabbed(true);
		}
		if(Mouse.isButtonDown(1)) {
			Mouse.setGrabbed(false);
		}

		if(Mouse.isGrabbed()) {
			if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
				Camera.moveCam(Direction.FORWARD);
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
				Camera.moveCam(Direction.BACKWARD);
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
				Camera.moveCam(Direction.LEFT);
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
				Camera.moveCam(Direction.RIGHT);
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
				Camera.moveCam(Direction.UP);
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
				Camera.moveCam(Direction.DOWN);
			}
			if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
				closeRequested = true;
			}

		}
	}
	
	public static void updateFPS() {
		if(getTime() - lastFPS > 1000) {
			Display.setTitle("Practice: " + fps + " fps");
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}
	
//	public static int getDelta() {
//		long time = getTime();
//		int delta = (int) (time - lastFrame);
//		lastFrame = time;
//		
//		return delta;
//	}
	
	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public static void main(String[] args) {
		mainLoop();
	}

}

