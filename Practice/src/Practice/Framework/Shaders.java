package Practice.Framework;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.lwjgl.opengl.GL20;

public class Shaders {

	private static int vertShader;
	private static int fragShader;
	
	public static void loadShaders(){
		Render.shaderProgram = glCreateProgram();
		
		try {
            vertShader = createShader("src/shader.vert", GL_VERTEX_SHADER);
            fragShader = createShader("src/shader.frag", GL_FRAGMENT_SHADER);
    	}
    	catch(Exception exc) {
    		exc.printStackTrace();
    		return;
    	}
    	finally {
    		if(vertShader == 0 || fragShader == 0)
    			return;
    	}
		
		glAttachShader(Render.shaderProgram, vertShader);
		glAttachShader(Render.shaderProgram, fragShader);
		glLinkProgram(Render.shaderProgram);
		glValidateProgram(Render.shaderProgram);
	}
	
	private static int createShader(String filename, int shaderType) throws Exception {
		int shader = 0;
		try {
			shader = glCreateShader(shaderType);
	        
			if(shader == 0)
	        	return 0;
	        
	        glShaderSource(shader, readFileAsString(filename));
	        glCompileShader(shader);
	        
	        if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE)
	            throw new RuntimeException("Error creating shader: " + getLogInfo(shader));
	        
	        return shader;
    	}
    	catch(Exception exc) {
    		glDeleteShader(shader);
    		throw exc;
    	}
    }
	
	private static String getLogInfo(int obj) {
        return glGetShaderInfoLog(obj, GL_INFO_LOG_LENGTH);
    }
    
    private static String readFileAsString(String filename) throws Exception {
        StringBuilder source = new StringBuilder();
        
        FileInputStream in = new FileInputStream(filename);
        
        Exception exception = null;
        
        BufferedReader reader;
        try{
            reader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
            
            Exception innerExc= null;
            try {
            	String line;
                while((line = reader.readLine()) != null)
                    source.append(line).append('\n');
            }
            catch(Exception exc) {
            	exception = exc;
            }
            finally {
            	try {
            		reader.close();
            	}
            	catch(Exception exc) {
            		if(innerExc == null)
            			innerExc = exc;
            		else
            			exc.printStackTrace();
            	}
            }
            
            if(innerExc != null)
            	throw innerExc;
        }
        catch(Exception exc) {
        	exception = exc;
        }
        finally {
        	try {
        		in.close();
        	}
        	catch(Exception exc) {
        		if(exception == null)
        			exception = exc;
        		else
					exc.printStackTrace();
        	}
        	
        	if(exception != null)
        		throw exception;
        }
        
        return source.toString();
    }
	
}
