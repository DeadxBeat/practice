package Practice.Framework;

import static org.lwjgl.opengl.GL11.GL_MODELVIEW_MATRIX;
import static org.lwjgl.opengl.GL11.GL_PROJECTION_MATRIX;
import static org.lwjgl.opengl.GL11.glGetFloat;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;


public abstract class Frustum {

	private static final int RIGHT = 0, LEFT = 1, BOTTOM = 2, TOP = 3, BACK = 4, FRONT = 5;
	private static final int A = 0, B = 1, C = 2, D = 3; 
	
	private static float[][] frustumData = new float[6][4];
	
	public static void getFrustum() {
		
		FloatBuffer projMatrix = BufferUtils.createFloatBuffer(16);
		FloatBuffer mviewMatrix = BufferUtils.createFloatBuffer(16);
		float clipMatrix[] = new float[16];
		
		glGetFloat(GL_PROJECTION_MATRIX, projMatrix);
		glGetFloat(GL_MODELVIEW_MATRIX, mviewMatrix);
		
		clipMatrix[0] = mviewMatrix.get(0) * projMatrix.get(0) + mviewMatrix.get(1) * projMatrix.get(4) + mviewMatrix.get(2) * projMatrix.get(8) + mviewMatrix.get(3) * projMatrix.get(12);
		clipMatrix[1] = mviewMatrix.get(0) * projMatrix.get(1) + mviewMatrix.get(1) * projMatrix.get(5) + mviewMatrix.get(2) * projMatrix.get(9) + mviewMatrix.get(3) * projMatrix.get(13);
		clipMatrix[2] = mviewMatrix.get(0) * projMatrix.get(2) + mviewMatrix.get(1) * projMatrix.get(6) + mviewMatrix.get(2) * projMatrix.get(10) + mviewMatrix.get(3) * projMatrix.get(14);
		clipMatrix[3] = mviewMatrix.get(0) * projMatrix.get(3) + mviewMatrix.get(1) * projMatrix.get(7) + mviewMatrix.get(2) * projMatrix.get(11) + mviewMatrix.get(3) * projMatrix.get(15);
		
		clipMatrix[4] = mviewMatrix.get(4) * projMatrix.get(0) + mviewMatrix.get(5) * projMatrix.get(4) + mviewMatrix.get(6) * projMatrix.get(8) + mviewMatrix.get(7) * projMatrix.get(12);
		clipMatrix[5] = mviewMatrix.get(4) * projMatrix.get(1) + mviewMatrix.get(5) * projMatrix.get(5) + mviewMatrix.get(6) * projMatrix.get(9) + mviewMatrix.get(7) * projMatrix.get(13);
		clipMatrix[6] = mviewMatrix.get(4) * projMatrix.get(2) + mviewMatrix.get(5) * projMatrix.get(6) + mviewMatrix.get(6) * projMatrix.get(10) + mviewMatrix.get(7) * projMatrix.get(14);
		clipMatrix[7] = mviewMatrix.get(4) * projMatrix.get(3) + mviewMatrix.get(5) * projMatrix.get(7) + mviewMatrix.get(6) * projMatrix.get(11) + mviewMatrix.get(7) * projMatrix.get(15);
		
		clipMatrix[8] = mviewMatrix.get(8) * projMatrix.get(0) + mviewMatrix.get(9) * projMatrix.get(4) + mviewMatrix.get(10) * projMatrix.get(8) + mviewMatrix.get(11) * projMatrix.get(12);
		clipMatrix[9] = mviewMatrix.get(8) * projMatrix.get(1) + mviewMatrix.get(9) * projMatrix.get(5) + mviewMatrix.get(10) * projMatrix.get(9) + mviewMatrix.get(11) * projMatrix.get(13);
		clipMatrix[10] = mviewMatrix.get(8) * projMatrix.get(2) + mviewMatrix.get(9) * projMatrix.get(6) + mviewMatrix.get(10) * projMatrix.get(10) + mviewMatrix.get(11) * projMatrix.get(14);
		clipMatrix[11] = mviewMatrix.get(8) * projMatrix.get(3) + mviewMatrix.get(9) * projMatrix.get(7) + mviewMatrix.get(10) * projMatrix.get(11) + mviewMatrix.get(11) * projMatrix.get(15);
		
		clipMatrix[12] = mviewMatrix.get(12) * projMatrix.get(0) + mviewMatrix.get(13) * projMatrix.get(4) + mviewMatrix.get(14) * projMatrix.get(8) + mviewMatrix.get(15) * projMatrix.get(12);
		clipMatrix[13] = mviewMatrix.get(12) * projMatrix.get(1) + mviewMatrix.get(13) * projMatrix.get(5) + mviewMatrix.get(14) * projMatrix.get(9) + mviewMatrix.get(15) * projMatrix.get(13);
		clipMatrix[14] = mviewMatrix.get(12) * projMatrix.get(2) + mviewMatrix.get(13) * projMatrix.get(6) + mviewMatrix.get(14) * projMatrix.get(10) + mviewMatrix.get(15) * projMatrix.get(14);
		clipMatrix[15] = mviewMatrix.get(12) * projMatrix.get(3) + mviewMatrix.get(13) * projMatrix.get(7) + mviewMatrix.get(14) * projMatrix.get(11) + mviewMatrix.get(15) * projMatrix.get(15);
		
		// Extract Sides
		
		// Right Side
		frustumData[RIGHT][A] = clipMatrix[3] - clipMatrix[0];
		frustumData[RIGHT][B] = clipMatrix[7] - clipMatrix[4];
		frustumData[RIGHT][C] = clipMatrix[11] - clipMatrix[8];
		frustumData[RIGHT][D] = clipMatrix[15] - clipMatrix[12];
		
		NormalizePlane(frustumData, RIGHT);
		
		// Left Side
		frustumData[LEFT][A] = clipMatrix[3] + clipMatrix[0];
		frustumData[LEFT][B] = clipMatrix[7] + clipMatrix[4];
		frustumData[LEFT][C] = clipMatrix[11] + clipMatrix[8];
		frustumData[LEFT][D] = clipMatrix[15] + clipMatrix[12];
		
		NormalizePlane(frustumData, LEFT);
		
		// Bottom Side
		frustumData[BOTTOM][A] = clipMatrix[3] + clipMatrix[1];
		frustumData[BOTTOM][B] = clipMatrix[7] + clipMatrix[5];
		frustumData[BOTTOM][C] = clipMatrix[11] + clipMatrix[9];
		frustumData[BOTTOM][D] = clipMatrix[15] + clipMatrix[13];
		
		NormalizePlane(frustumData, BOTTOM);
		
		// Top Side
		frustumData[TOP][A] = clipMatrix[3] - clipMatrix[1];
		frustumData[TOP][B] = clipMatrix[7] - clipMatrix[5];
		frustumData[TOP][C] = clipMatrix[11] - clipMatrix[9];
		frustumData[TOP][D] = clipMatrix[15] - clipMatrix[13];
		
		NormalizePlane(frustumData, TOP);
		
		// Back Side 
		frustumData[BACK][A] = clipMatrix[3] - clipMatrix[2];
		frustumData[BACK][B] = clipMatrix[7] - clipMatrix[6];
		frustumData[BACK][C] = clipMatrix[11] - clipMatrix[10];
		frustumData[BACK][D] = clipMatrix[15] - clipMatrix[14];
		
		NormalizePlane(frustumData, BACK);
		
		// Front Side
		frustumData[FRONT][A] = clipMatrix[3] + clipMatrix[2];
		frustumData[FRONT][B] = clipMatrix[7] + clipMatrix[6];
		frustumData[FRONT][C] = clipMatrix[11] + clipMatrix[10];
		frustumData[FRONT][D] = clipMatrix[15] + clipMatrix[14];
		
		NormalizePlane(frustumData, FRONT);
	}
	
	private static void NormalizePlane(float[][] fData, int side) {
		float magnitude = (float)Math.sqrt(fData[side][A] * fData[side][A] + fData[side][B] * fData[side][B] + fData[side][C] * fData[side][C]);
		
		fData[side][A] /= magnitude;
		fData[side][B] /= magnitude;
		fData[side][C] /= magnitude;
		fData[side][D] /= magnitude;
		
		frustumData = fData;
	}
	
	public static boolean pointInFrustum(float x, float y, float z) {
		for(int i = 0; i < 6; i++ ) {
			if(frustumData[i][A] * x + frustumData[i][B] * y + frustumData[i][C] * z + frustumData[i][D] <= 0)
			{
				// The point was behind a side, so it ISN'T in the frustum
				return false;
			}
		}
		
		return true;
	}
	
	
	public static boolean cubeInFrustum(float x, float y, float z, float size) {
		for(int i = 0; i < 6; i++) {
			if(frustumData[i][A] * (x-size) + frustumData[i][B] * (y-size) + frustumData[i][C] * (z-size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x+size) + frustumData[i][B] * (y-size) + frustumData[i][C] * (z-size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x-size) + frustumData[i][B] * (y+size) + frustumData[i][C] * (z-size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x+size) + frustumData[i][B] * (y+size) + frustumData[i][C] * (z-size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x-size) + frustumData[i][B] * (y-size) + frustumData[i][C] * (z+size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x+size) + frustumData[i][B] * (y-size) + frustumData[i][C] * (z+size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x-size) + frustumData[i][B] * (y+size) + frustumData[i][C] * (z+size) + frustumData[i][D] > 0)
				continue;
			if(frustumData[i][A] * (x+size) + frustumData[i][B] * (y+size) + frustumData[i][C] * (z+size) + frustumData[i][D] > 0)
				continue;
			
			return false;
		}
		
		return true;
	}
	
}
