package Practice.Framework;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;


public class Triangle {

	public void draw(){
		glUseProgram(Render.shaderProgram);
		//glLoadIdentity();
		//red, which will show if our fragment shader doesn't work
		glColor3f(1.0f, 1.0f, 1.0f);

		drawImmediate();
		glUseProgram(0);
	}
	
	public void drawImmediate(){
		glBegin(GL_TRIANGLES);
		  glVertex3f(-1.0f, 0.0f, 0.0f);
		  glVertex3f(0.0f, 2.0f, 0.0f);
		  glVertex3f(1.0f, 0.0f, 0.0f);
		glEnd();
	}
	
}
