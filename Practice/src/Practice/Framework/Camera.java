package Practice.Framework;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluPerspective;

import Practice.Framework.Enums.Direction;

public abstract class Camera {
	
	private static float moveSpeed = 0.15f;
	private static float maxLookDown = -90.0f;
	private static float maxLookUp = 90.0f;
	
	public static Vector3f camPos = new Vector3f(0.0f, 1.0f, 10.0f);
	public static Vector3f camRot = new Vector3f(0.0f, 0.0f, 0.0f);
	

	public static void setPerspective() {
		int prevMatrixMode = glGetInteger(GL_MATRIX_MODE);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0f, ((float)Display.getWidth() / (float)Display.getHeight()), 0.001f, 1000.0f);
		glMatrixMode(prevMatrixMode);
	}
	
	public static void setOrtho() {
		int prevMatrixMode = glGetInteger(GL_MATRIX_MODE);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0f, (float)Display.getWidth(),(float)Display.getHeight(), 0.0f, -1.0f, 1.0f);
		glMatrixMode(prevMatrixMode);
	}
	
	public static void lookAt(float mouseDX, float mouseDY) {
		if(Mouse.isGrabbed()) {
			mouseDX *= moveSpeed;
			mouseDY *= moveSpeed;
			float pitch = camRot.x;
	        float yaw = camRot.y;
	        
	        if (yaw + mouseDX >= 360) {
	            yaw = yaw + mouseDX - 360;
	            camRot.y = yaw;
	        } else if (yaw + mouseDX < 0) {
	            yaw = 360 - yaw + mouseDX;
	            camRot.y = yaw;
	        } else {
	            yaw += mouseDX;
	            camRot.y = yaw;
	        }
	        if (pitch - mouseDY >= maxLookDown
	                && pitch - mouseDY <= maxLookUp) {
	            pitch += -mouseDY;
	            camRot.x = pitch;
	        } else if (pitch - mouseDY < maxLookDown) {
	            pitch = maxLookDown;
	            camRot.x = pitch;
	        } else if (pitch - mouseDY > maxLookUp) {
	            pitch = maxLookUp;
	            camRot.x = pitch;
	        }
		}
	}

	public static void update() {
		glRotatef(camRot.x, 1.0f, 0.0f, 0.0f);
		glRotatef(camRot.y, 0.0f, 1.0f, 0.0f);
		glRotatef(camRot.z, 0.0f, 0.0f, 1.0f);
		glTranslatef(-camPos.x, -camPos.y, -camPos.z);
		
	}
	
	public static void moveCam(Direction direction) {
		float dx = 0;
		float dy = 0;
		float dz = 0;
		switch(direction) {
			case FORWARD:
				dz -= 1;
				break;
				
			case BACKWARD:
				dz += 1;
				break;
				
			case LEFT:
				dx -= 1;
				break;
				
			case RIGHT:
				dx += 1;
				break;
				
			case UP:
				dy += 1;
				break;
				
			case DOWN:
				dy -= 1;
				break;
		}
		
		float speedX = dx * moveSpeed;
    	float speedZ = dz * moveSpeed;
        camPos.z += speedX * (float) Math.cos(Math.toRadians(camRot.y - 90)) + speedZ * Math.cos(Math.toRadians(camRot.y));
        camPos.x -= speedX * (float) Math.sin(Math.toRadians(camRot.y - 90)) + speedZ * Math.sin(Math.toRadians(camRot.y));
        camPos.y += dy * moveSpeed;
	}
	
}
