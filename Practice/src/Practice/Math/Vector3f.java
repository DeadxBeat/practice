package Practice.Math;

/** Vector class (using float).
 */

public class Vector3f
{

        
          /** Vector X. **/
        public float x;
        
          /** Vector Y. **/
        public float y;
        
          /** Vector Z. **/
        public float z;


        /** Vector3f constructor.
         *  @param x Vector X.
         *  @param y Vector Y.
         *  @param z Vector Z.
         */

        public Vector3f(float x, float y, float z)
        {
                setTo(x, y, z);
        }
        
    /**
        Creates a new Vector3D with the same values as the
        specified Vector3D.
    */
    public Vector3f(Vector3f v) 
        {
                setTo(v);
        }

    /** Vector3f constructor.
         */
    public Vector3f()
        {
                this.x = 0.0f;
                this.y = 0.0f;
                this.z = 0.0f;
        }
    
    /** 
     * Sets each coordinate to the same coordinate as the Vector3f object passed in.
     * @param v Vector3f
     */
    public void setTo(Vector3f v) 
        {
                this.x = v.x;
                this.y = v.y;
                this.z = v.z;
        
        }
    
    /**
     * Sets each coordinate to the passed in values.
     * @param x 
     * @param y 
     * @param z 
     */
    public void setTo(float x, float y, float z) 
        {
                this.x = x;
                this.y = y;
                this.z = z;

        }
    /**
     * Sets all coordinates of current object to zero.
     */
    public void setZero()
    {
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = 0.0f;
    
    }
    
    /**
     * Sets all coordinates of current object to their opposite value.
     */
    public void negate()
    {
        this.x = - this.x;
        this.y = - this.y;
        this.z = - this.z;
    }
    
    /**
     * Returns a string representing all of the actual coordinate values.
     */
    public String toString()
    {
        return new String("X:" + x + " Y:" + y + " Z:" + z);
    }
    

}
